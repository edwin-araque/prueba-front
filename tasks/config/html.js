module.exports = function (grunt) {
  grunt.config.set('htmlmin', {
   dist: {                                      // Target
      options: {                                 // Target options
        removeComments: true,
        collapseWhitespace: true
      },
      files: {                                   // Dictionary of files
        'index.html': 'resources/views/html/index.html',
        'agenda.html': 'resources/views/html/agenda.html',
        'conferencistas.html': 'resources/views/html/conferencistas.html',
        'congreso.html': 'resources/views/html/congreso.html',
        'inscripciones.html': 'resources/views/html/inscripciones.html',
        'contacto.html': 'resources/views/html/contacto.html',
        'muestra.html': 'resources/views/html/muestra.html', 
        'galeria.html': 'resources/views/html/galeria.html', 
        'patrocinadores.html': 'resources/views/html/patrocinadores.html' 
      }
    }
  }); 

  grunt.loadNpmTasks('grunt-contrib-htmlmin');
};