module.exports = function(grunt) {
  grunt.config.set('watch', {
    js_core: {
      files: ['resources/source/js/core/**/*.js'],
      tasks: ['concat:extend', 'concat:core_extend', 'uglify:core', 'notify:js_core']
    },
    js_app: {
      files: ['resources/source/js/*.js'],
      tasks: ['uglify:app', 'notify:js_app']
    },
    handlebars: {
      files: ['resources/source/handlebars/**/*.handlebars'],
      tasks: ['handlebars:compile', 'uglify:templates', 'notify:handlebars']
    },
    sass: {
      files: ['resources/source/sass/**/*.scss'],
      tasks: ['sass', 'notify:sass']
    },
    sprites: {
      files: ['resources/source/sprites/*.png'],
      tasks: ['sprite:all', 'notify:sprites']
    },
    svgs: {
      files: ['resources/source/svgs/*.svg'],
      tasks: ['svgzr:dist', 'notify:svgs']
    },
    codekit: {  
      files: ['resources/views/*.kit'],
      tasks: ['codekit', 'notify:codekit']
    },
    htmlmin: {  
      files: ['resources/views/html/*.html'],
      tasks: ['htmlmin', 'notify:htmlmin']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
};
