module.exports = function (grunt) {
  grunt.config.set('codekit', {
      globbed_example_config : {
        src : 'resources/views/*.kit',
        dest : 'resources/views/html'  
    } 
  }); 

  grunt.loadNpmTasks('grunt-codekit');
};