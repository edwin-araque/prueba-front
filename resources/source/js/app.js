$(document).ready(function(){

  $('.audi-uno').slick({
    dots: true,
    infinite: true,
    speed: 500,
    fade: true,
    cssEase: 'linear',
    nextArrow: '.next',
    prevArrow: '.prev'
  });
 

  $('.tabs').click(function(){
    $('.tabs').removeClass('active');
    $(this).addClass('active');
    var itemdiv = $(this).attr('data-div'); 
    $('.itemdiv').hide();
    $(itemdiv).fadeIn();
  });

  $(".mini").click(function(){
    $(".audi-dos").hide();
    $(".audi-uno").show();
    $(".mini").css("border","1px solid #635b55");
    $(".mini-dos").css("border","none");
    $('.audi-dos').slick('unslick'); 
    $('.audi-uno').slick({
      dots: true,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear',
      nextArrow: '.next',
      prevArrow: '.prev'
    });
  });
  $(".mini-dos").click(function(){
    $(".audi-uno").hide();
    $(".audi-dos").show();
    $(".mini-dos").css("border","1px solid #635b55");
    $(".mini").css("border","none");
    $('.audi-uno').slick('unslick'); 
    $('.audi-dos').slick({
      dots: true,
      infinite: true,
      speed: 500,
      fade: true,
      cssEase: 'linear',
      nextArrow: '.next',
      prevArrow: '.prev'
    });
  });


  $(".detalle").click(function(){
  	$(".content-textos").show();
  	$(".content-audi").show();
    $(".content-audi-dos").hide();
    $(".content-tarjeta").hide();
  });

  $(".comprar").click(function(){
  	$(".content-tarjeta").show();
    $(".content-textos").hide();
    $(".content-audi").hide();
    $(".content-audi-dos").show();
  });

  $(".continuar").click(function(){
    $(".formu-uno").hide();
    // $(".continuar").hide();
    $(".formu-dos").show();

  });
  $(".cancelar").click(function(){
    $(".formu-dos").hide();
    $(".formu-uno").show();
    // $(".continuar").show();
  });

  $('.popup').click(function(){
    $('.pop-up').show();
    });
  
    $(".equis").click(function(){
    $(".pop-up").hide()
    });

    $(".aceptar").click(function(){
    $(".pop-up").hide()
    });
});